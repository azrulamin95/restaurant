import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import SearchBar from '../components/SearchBar';
import ResultList from '../components/ResultList';
import useResults from '../hooks/useResults';

const SearchScreen = () => {
	const [term, setTerm] = useState('');
	const [searchApi,result,errMsg] = useResults();

	const filterResultByPrice = (price) => {
		return result.filter(result => {
			return result.price === price;
		});
	};

	return (
		<View>
			<SearchBar 
				term={term} 
				onTermChange={newTerm => setTerm(newTerm)} 
				onTermSubmit={() => searchApi(term)}
			/>
			{ errMsg ? <Text>{errMsg}</Text> : null }
			<Text>We Have found {result.length} results</Text>

			<ResultList results={filterResultByPrice('$')} title="Cost Effective"/>
			<ResultList results={filterResultByPrice('$$')} title="Bit Pricer"/>
			<ResultList results={filterResultByPrice('$$$')} title="Big Spender"/>
		</View>
	);
};

const styles = StyleSheet.create({

});

export default SearchScreen;