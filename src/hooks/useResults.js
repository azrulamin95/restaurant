import { useEffect, useState } from 'react';
import yelp from '../api/yelp';

export default () => {
	const [result, setresult] = useState([]);
	const [errMsg, setErrMsg] = useState('');
	
	const searchApi = async (SearchTerm) => {
		console.log('hello world');
		try{
			const response = await yelp.get('/search',{
				params: {
					limit: 50,
					term: SearchTerm,
					location: 'san jose'
				}
			});
			setresult(response.data.businesses);
		}catch(err){
			setErrMsg('Something went wrong');
		}
	};
	// san jose
	// searchApi('pasta');

	useEffect(()=>{
		searchApi('pasta');
	}, []);

	return [searchApi,result,errMsg];
};